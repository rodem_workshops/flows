# RODEM flows workshop

Some basic notebooks for creating and training flows, and using the nflows package.

These notebooks should run in a google colab environment.

This repository contains three notebooks.

## Contents

[AffineCouplingFlow](https://gitlab.cern.ch/rodem_workshops/flows/-/blob/master/AffineCouplingFlow.ipynb) - coding affine coupling flows from scratch in pytorch with detailed implementation notes.

[nflows](https://gitlab.cern.ch/rodem_workshops/flows/-/blob/master/nflows.ipynb) - a notebook that walks through the use of the [nflows](https://github.com/bayesiains/nflows/tree/639c3a771d57c29a27c307140cc94a1008ee9f55/nflows) package.

[DIY](https://gitlab.cern.ch/rodem_workshops/flows/-/blob/master/DIY.ipynb) - a notebook where the user is required to define a flow themselves using the nflows package, and build a uniform base distribution by hand.
